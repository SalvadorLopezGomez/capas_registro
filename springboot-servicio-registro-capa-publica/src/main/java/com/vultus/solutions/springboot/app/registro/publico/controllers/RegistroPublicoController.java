package com.vultus.solutions.springboot.app.registro.publico.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vultus.solutions.springboot.app.commons.models.entity.User;
import com.vultus.solutions.springboot.app.registro.publico.models.service.IRegistroPublicService;

@RestController
public class RegistroPublicoController {

	@Autowired
	@Qualifier("serviceFeign")
	private IRegistroPublicService iPublicoService;
	
	@GetMapping("/listar")
	public ResponseEntity<?> getUsers(){
		ResponseEntity<User> users = (ResponseEntity<User>) iPublicoService.findAll();
		Map<String, String> json =  new HashMap<>();
		
		if(users == null) {
			json.put("users", "Lista vacia");
			return new ResponseEntity<>(json, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(users, HttpStatus.OK);
		}
	}
	
	@GetMapping("/detalle/{id}")
	public ResponseEntity<?> getUser(@PathVariable Long id){
		ResponseEntity<?> user = iPublicoService.findById(id);
		Map<String, String> json =  new HashMap<>();
		if(user == null) {
			json.put("user", "El usuario no existe");
			return new ResponseEntity<>(json, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(user, HttpStatus.OK);
		}
	}
	
	@PostMapping("/add")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> addUser(@RequestBody User user){
		ResponseEntity<?> users = iPublicoService.createUser(user);
		ObjectMapper oMapper = new ObjectMapper();
		Map<String, String> json =  new HashMap<>();
		if(users == null) {
			json.put("user", "El usuario no pudo registrarse");
		}else {
			json = oMapper.convertValue(users, Map.class);
			json.put("info", "El usuario se ha registrado");
		}
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.CREATED);
	}
	
	@PutMapping("/actualizar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> editUser(@RequestBody User user, @PathVariable Long id){
		ResponseEntity<?> users = iPublicoService.updateUser(user, id);
		Map<String, String> json =  new HashMap<>();
		ObjectMapper oMapper = new ObjectMapper();
		if(users == null) {
			json.put("user", "El usuario no se pudo modificar");
		}else {
			json = oMapper.convertValue(users, Map.class);
			json.put("info", "El usuario se ha modificado");
		}
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/quitar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> borrar(@PathVariable Long id){
		iPublicoService.deleteUser(id);
		Map<String, String> json =  new HashMap<>();
		json.put("user", "El usuario se ha eliminado");
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.NO_CONTENT);
	}
}
