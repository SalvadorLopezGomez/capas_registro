package com.vultus.solutions.springboot.app.registro.publico.models.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.vultus.solutions.springboot.app.registro.publico.cliente.RegistroClienteRest;
import com.vultus.solutions.springboot.app.commons.models.entity.User;

@Service("serviceFeign")
@Primary
public class RegistroPublicoFeign implements IRegistroPublicService {
	
	@Autowired
	private RegistroClienteRest clienFeign;

	@Override
	public ResponseEntity<?> findAll() {
		return clienFeign.findAll();
	}

	@Override
	public ResponseEntity<?> findById(Long id) {
		return clienFeign.getUser(id);
	}

	@Override
	public ResponseEntity<?> createUser(User user) {
		return clienFeign.newUser(user);
	}

	@Override
	public ResponseEntity<?> updateUser(User user, Long id) {
		return clienFeign.editUser(user, id);
	}

	@Override
	public ResponseEntity<?> deleteUser(Long id) {
		return clienFeign.deleteUser(id);
	}

}
