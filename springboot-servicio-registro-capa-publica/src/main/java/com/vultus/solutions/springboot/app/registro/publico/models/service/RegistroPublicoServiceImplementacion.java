package com.vultus.solutions.springboot.app.registro.publico.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.vultus.solutions.springboot.app.commons.models.entity.User;

@Service
public class RegistroPublicoServiceImplementacion implements IRegistroPublicService {
	
	@Autowired
	private RestTemplate clientRest;

	@Override
	public ResponseEntity<?> findAll() {
		ResponseEntity<?> users = (ResponseEntity<?>) Arrays.asList(clientRest.getForObject("http://localhost:8007/usuario/list", JSONPObject[].class));
		return users;
	}

	@Override
	public ResponseEntity<?> findById(Long id) {
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		ResponseEntity<?> users = clientRest.getForObject("http://localhost:8007/usuario/{id}", ResponseEntity.class, pathVariables);
		return users;
	}

	@Override
	public ResponseEntity<?> createUser(User user) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> updateUser(User user, Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> deleteUser(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
