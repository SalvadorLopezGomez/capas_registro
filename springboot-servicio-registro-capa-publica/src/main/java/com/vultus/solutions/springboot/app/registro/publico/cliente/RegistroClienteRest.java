package com.vultus.solutions.springboot.app.registro.publico.cliente;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.vultus.solutions.springboot.app.commons.models.entity.User;

@FeignClient(name="servicio-registro-negocio")
public interface RegistroClienteRest {

	@GetMapping("/usuario/list")
	public ResponseEntity<?> findAll();
	
	@GetMapping("usuario/{id}")
	public ResponseEntity<?> getUser(@PathVariable Long id);
	
	@PostMapping("/register")
	public ResponseEntity<?> newUser(@RequestBody User user);
	
	@PutMapping("/editar/{id}")
	public ResponseEntity<?> editUser(@RequestBody User user, @PathVariable Long id);
	
	@DeleteMapping("/borrar/{id}")
	public ResponseEntity<?> deleteUser(@PathVariable Long id);
}
