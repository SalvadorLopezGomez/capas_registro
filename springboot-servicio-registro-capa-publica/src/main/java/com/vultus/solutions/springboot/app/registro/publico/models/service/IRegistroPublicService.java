package com.vultus.solutions.springboot.app.registro.publico.models.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.vultus.solutions.springboot.app.commons.models.entity.User;

public interface IRegistroPublicService {

	public ResponseEntity<?> findAll();
	public ResponseEntity<?> findById(Long id);
	public ResponseEntity<?> createUser(User user);
	public ResponseEntity<?> updateUser(User user, Long id);
	public ResponseEntity<?> deleteUser(Long id);
}
