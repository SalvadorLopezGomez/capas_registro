package com.vultus.solutions.springboot.app.login;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EntityScan({"com.vultus.solutions.springboot.app.commons.models.entity"})
public class SpringbootServicioLoginApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioLoginApplication.class, args);
	}

}
