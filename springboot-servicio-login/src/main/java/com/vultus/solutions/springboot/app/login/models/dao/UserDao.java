package com.vultus.solutions.springboot.app.login.models.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.vultus.solutions.springboot.app.commons.models.entity.User;

@RepositoryRestResource(path = "users/login")
public interface UserDao extends PagingAndSortingRepository<User, Long>{

	
	public User findByUsername(@Param("name") String username);
	
	@Query("select u from User u where u.name=?1")
	public User obtenerPorName(String name);
}
