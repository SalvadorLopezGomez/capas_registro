package com.vultus.solutions.springboot.app.registro.negocio.models.service;

import java.util.List;

import com.vultus.solutions.springboot.app.commons.models.entity.User;

public interface InterfaceRegistroService {

	public List<User> findAll();
	public User findById(Long id);
	public User createUser(User user);
	public User updateUser(User user, Long id);
	public void deleteUser(Long id);
}
