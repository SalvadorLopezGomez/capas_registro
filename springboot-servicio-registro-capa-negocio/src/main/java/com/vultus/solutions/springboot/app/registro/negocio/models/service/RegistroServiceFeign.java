package com.vultus.solutions.springboot.app.registro.negocio.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.vultus.solutions.springboot.app.registro.negocio.clientes.PesistenciaClientRest;
import com.vultus.solutions.springboot.app.commons.models.entity.User;

@Service("serviceFeign")
@Primary
public class RegistroServiceFeign implements InterfaceRegistroService {
	
	@Autowired
	private PesistenciaClientRest clientFeign;

	@Override
	public List<User> findAll() {
		return clientFeign.listUsers();
	}

	@Override
	public User findById(Long id) {
		return clientFeign.getUser(id);
	}

	@Override
	public User createUser(User user) {
		return clientFeign.newUser(user);
	}

	@Override
	public User updateUser(User user, Long id) {
		return clientFeign.editUser(user, id);
	}

	@Override
	public void deleteUser(Long id) {
		clientFeign.deleteUser(id);
	}

}
