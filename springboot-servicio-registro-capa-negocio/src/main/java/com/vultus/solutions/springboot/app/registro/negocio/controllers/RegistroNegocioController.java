package com.vultus.solutions.springboot.app.registro.negocio.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.vultus.solutions.springboot.app.commons.models.entity.User;
import com.vultus.solutions.springboot.app.registro.negocio.models.service.InterfaceRegistroService;

@RestController
public class RegistroNegocioController {

	@Autowired
	@Qualifier("serviceFeign")
	private InterfaceRegistroService iRegistroService;
	
	@GetMapping("/usuario/list")
	public ResponseEntity<?> getUsers(){
		List<User> users = iRegistroService.findAll();
		Map<String, String> json =  new HashMap<>();
		
		if(users == null && users.isEmpty()) {
			json.put("users", "Lista vacia");
			return new ResponseEntity<>(json, HttpStatus.OK);
		}else {
			return new ResponseEntity<>(users, HttpStatus.OK);
		}
	}
	
	@GetMapping("/usuario/{id}")
	public ResponseEntity<?> getUser(@PathVariable Long id){
		User user = iRegistroService.findById(id);
		Map<String, String> json =  new HashMap<>();
		ObjectMapper oMapper = new ObjectMapper();
		if(user == null) {
			json.put("user", "El usuario no existe");
		}else {
			json = oMapper.convertValue(user, Map.class);
		}
		return new ResponseEntity<>(json, HttpStatus.OK);
	}
	
	@PostMapping("/register")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> newUser(@RequestBody User user) {
		User users = iRegistroService.createUser(user);
		Map<String, String> json =  new HashMap<>();
		ObjectMapper oMapper = new ObjectMapper();
		if(users == null) {
			json.put("user", "El usuario no pudo registrarse");
		}else {
			json = oMapper.convertValue(users, Map.class);
			json.put("info", "El usuario se ha registrado");
		}
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.CREATED);
	}
	
	@PutMapping("/editar/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> editUser(@RequestBody User user, @PathVariable Long id) {
		User users = iRegistroService.updateUser(user, id);
		Map<String, String> json =  new HashMap<>();
		ObjectMapper oMapper = new ObjectMapper();
		if(users == null) {
			json.put("user", "El usuario no se pudo modificar");
		}else {
			json = oMapper.convertValue(users, Map.class);
			json.put("info", "El usuario se ha modificado");
		}
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/borrar/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public ResponseEntity<?> deleteUser(@PathVariable Long id) {
		iRegistroService.deleteUser(id);
		Map<String, String> json =  new HashMap<>();
		json.put("user", "El usuario se ha eliminado");
		return new ResponseEntity<Map<String, String>>(json, HttpStatus.NO_CONTENT);
	}
}
