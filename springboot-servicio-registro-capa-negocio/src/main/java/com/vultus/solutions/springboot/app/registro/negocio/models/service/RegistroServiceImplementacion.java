package com.vultus.solutions.springboot.app.registro.negocio.models.service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.vultus.solutions.springboot.app.commons.models.entity.User;

@Service("serviceRestTemplate")
public class RegistroServiceImplementacion implements InterfaceRegistroService {

	@Autowired
	private RestTemplate clientRest;
	
	@Override
	public List<User> findAll() {
		List<User> users = Arrays.asList(clientRest.getForObject("http://localhost:8006/users/list", User[].class));
		return users;
	}

	@Override
	public User findById(Long id) {
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		User users = clientRest.getForObject("http://localhost:8006/users/user/{id}", User.class, pathVariables);
		return users;
	}

	@Override
	public User createUser(User user) {
		HttpEntity<User> body = new HttpEntity<User>(user);
		ResponseEntity<User> response = clientRest.exchange("http://localhost:8006/users/register", HttpMethod.POST, body, User.class);
		User userResponse = response.getBody();
		return userResponse;
	}

	@Override
	public User updateUser(User user, Long id) {
		HttpEntity<User> body = new HttpEntity<User>(user);
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		ResponseEntity<User> response = clientRest.exchange("http://localhost:8006/users/update/{id}", HttpMethod.PUT, body, User.class, pathVariables);
		return response.getBody();
	}

	@Override
	public void deleteUser(Long id) {
		Map<String, String> pathVariables = new HashMap<String, String>();
		pathVariables.put("id", id.toString());
		clientRest.delete("http://localhost:8006/users/delete/{id}", pathVariables);
	}

}
