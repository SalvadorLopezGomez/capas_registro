package com.vultus.solutions.springboot.app.registro.negocio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableCircuitBreaker
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class SpringbootServicioRegistroCapaNegocioApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioRegistroCapaNegocioApplication.class, args);
	}

}
