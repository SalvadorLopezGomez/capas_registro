package com.vultus.solutions.springboot.app.registro.negocio.clientes;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.vultus.solutions.springboot.app.commons.models.entity.User;

@FeignClient(name = "servicio-registro-persistencia")
public interface PesistenciaClientRest {

	@GetMapping("/users/list")
	public List<User> listUsers();
	
	@GetMapping("users/user/{id}")
	public User getUser(@PathVariable Long id);
	
	@PostMapping("/users/register")
	public User newUser(@RequestBody User user);
	
	@PutMapping("/users/update/{id}")
	public User editUser(@RequestBody User user, @PathVariable Long id);
	
	@DeleteMapping("/users/delete/{id}")
	public void deleteUser(@PathVariable Long id);
}
