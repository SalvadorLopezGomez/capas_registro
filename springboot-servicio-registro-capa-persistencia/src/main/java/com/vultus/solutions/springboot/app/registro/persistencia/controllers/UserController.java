package com.vultus.solutions.springboot.app.registro.persistencia.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.vultus.solutions.springboot.app.commons.models.entity.User;
import com.vultus.solutions.springboot.app.registro.persistencia.models.service.InterfaceUserService;

@RestController
public class UserController {

	@Autowired
	private InterfaceUserService iuserService;
	
	@GetMapping("/users/list")
	public List<User> listUsers(){
		return iuserService.findAll();
	}
	
	@GetMapping("/users/user/{id}")
	public User getUser(@PathVariable Long id) {
		return iuserService.findById(id);
	}
	
	@PostMapping("/users/register")
	@ResponseStatus(HttpStatus.CREATED)
	public User newUser(@RequestBody User user) {
		return iuserService.createUser(user);
	}
	
	@PutMapping("/users/update/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public User editUser(@RequestBody User user, @PathVariable Long id) {
		User users = iuserService.findById(id);
		users.setName(user.getName());
		users.setFsurname(user.getFsurname());
		users.setSsurname(user.getSsurname());
		users.setEmail(user.getEmail());
		users.setPhone(user.getPhone());
		users.setPassword(user.getPassword());
		return iuserService.createUser(users);
	}
	
	@DeleteMapping("/users/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteUser(@PathVariable Long id) {
		iuserService.deleteById(id);
	}
}
