package com.vultus.solutions.springboot.app.registro.persistencia.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vultus.solutions.springboot.app.commons.models.entity.User; 
import com.vultus.solutions.springboot.app.registro.persistencia.models.dao.UserDao;

@Service
public class UserServiceImplementation implements InterfaceUserService{
	
	@Autowired
	private UserDao userDao;

	@Override
	@Transactional(readOnly = true)
	public List<User> findAll() {
		return (List<User>) userDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public User findById(Long id) {
		return userDao.findById(id).orElse(null);
	}

	@Override
	@Transactional
	public User createUser(User user) {
		return userDao.save(user);
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		userDao.deleteById(id);
	}
}
