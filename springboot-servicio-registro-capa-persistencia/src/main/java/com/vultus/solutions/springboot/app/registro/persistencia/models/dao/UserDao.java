package com.vultus.solutions.springboot.app.registro.persistencia.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.vultus.solutions.springboot.app.commons.models.entity.User;


public interface UserDao extends CrudRepository<User, Long>{

}
