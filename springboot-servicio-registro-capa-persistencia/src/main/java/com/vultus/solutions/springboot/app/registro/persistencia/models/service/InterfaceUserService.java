package com.vultus.solutions.springboot.app.registro.persistencia.models.service;

import java.util.List;

import com.vultus.solutions.springboot.app.commons.models.entity.User;


public interface InterfaceUserService {

	public List<User> findAll();
	public User findById(Long id);
	public User createUser(User user);
	public void deleteById(Long id);
}
