package com.vultus.solutions.springboot.app.registro.persistencia;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
@EntityScan({"com.vultus.solutions.springboot.app.commons.models.entity"})
public class SpringbootServicioRegistroCapaPersistenciaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootServicioRegistroCapaPersistenciaApplication.class, args);
	}

}
